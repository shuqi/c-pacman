#include <allegro.h>
#include <stdio.h>
#include <cstdlib>

// Constante
#define MAX_ROWS 20
#define MAX_COLUMNS 31

// Global variables
BITMAP *bmScreen;
BITMAP *bmImageBoard;
BITMAP *bmBufferPacman;
BITMAP *bmPacman;
BITMAP *bmFood;
BITMAP *labVertical;
BITMAP *labHorizontal;
BITMAP *labarribaIzquierda;
BITMAP *labarribaDerecha;
BITMAP *lababajoIzquierda;
BITMAP *lababajoDerecha;
BITMAP *bmPhantom;
BITMAP *bmPhantomBlue;
BITMAP *bmBufferPhantom;
BITMAP *bmGameOver;
BITMAP *bmLetras;
BITMAP *bmBufferLetras;
BITMAP *bmNumeros;
BITMAP *bmBufferNumeros;
BITMAP *powerFood;
BITMAP *bmcherry;

SAMPLE *bolitas;//Musica cuando como las bolitas
SAMPLE *muerte;

int opcion = 0;//Opcion para el menú

int dir=4;//Dirección de pacman

int px=32*13;//Posicion en x de pacman
int py=32*10;//Posicion en y de pacman
int fx = 32*14, fy = 32*14;
int antpx, antpy;
int f = 0;
int puntaje = 0;
int vida = 5;
bool azul = false;
bool cherry = true;

char board [MAX_ROWS][MAX_COLUMNS]={
    "a*************************b",
    "+?00-0000-a*****b-0000-00?+",
    "+ ab0a**b0+     +0a**b0ab +",
    "+ cd0c**d0c     d0c**d0cd +",
    "+-00-00-00-00000-00-00-00-+",
    "+ ** ab ****a*b**** ab ** +",
    "+ ab cd     c*d     cd ab +",
    "+ ++   -000-000-000-   ++ +",
    "  ++-ab a*********b ab-++  ",
    "+0++0++0c*********d0++0++0+",
    "+0cd0cd-000-0 0-000-cd0cd0+",
    "+-00-ab-000-a*b-000-ab-00-+",
    "+0ab0cd00-0-c*d-00-0cd0ab0+",
    "+0++0a**b *      * a*b0++0+",
    "+0cd0c**d-00000000-c*d0cd0+",
    "+?0000000-********-000000?+",
    "c*************************d"
    };

// Function prototypes
void drawBoard();
void allegroStart(const char* title,int width,int height);
void drawPacman();
void movePhantom();
void moveRight(int address);
void moveLeft(int address);
void moveUp(int address);
void moveDown(int address);
bool gameOver();
void pantalla();
void drawPhantom();
void insertarPalabra(char pal[],int posX,int posY);
int largo_cadena(char cadena[]);
int pixelLetra(char letra);
void insertarNumero(char pal[],int posX,int posY);
int pixelNumero(char numero);

/**
* Clase interna para la creación del menú principal
* @autor Marco Mendoza.
* @create date 03/06/2015
*/
class menuP{
    BITMAP *menu1, *menu2, *menu3, *buffer, *raton, *fondo;
    int ancho, alto;

    public:
        menuP(int an, int al, char *n1, char *n2, char *n3, char *n4);
        void pintar();
};

/**
* Constructor de la clase menuP
* @autor Marco Mendoza.
* @param an ancho del menú
* @param al alto del menú
* @param *n1 primer fondo del menú
* @param *n2 segundo fondo del menú
* @param *n3 puntero del mouse
* @param *n4 tercer fondo del menú
* @create date 03/06/2015
*/
menuP::menuP(int an, int al, char *n1, char *n2, char *n3, char *n4){
    ancho = an;
    alto = al;
    menu1 = load_bitmap(n1,NULL);
    menu2 = load_bitmap(n2,NULL);
    menu3 = load_bitmap(n4,NULL);
    raton = load_bitmap(n3,NULL);
    buffer = create_bitmap(ancho, alto);
    fondo = menu1;
}

/**
* Dibuja el menú principal y valida las opciones del menú
* @autor Marco Mendoza.
* @create date 03/06/2015
*/
void menuP::pintar(){
    bool salida = false;
    while(!salida && !key[KEY_ESC]){
        if(mouse_x > 400 && mouse_x < 525 && mouse_y > 250 && mouse_y < 300){
            fondo = menu2;
            if(mouse_b & 1){
                salida = true;
                opcion = 1;
            }
        } else {fondo = menu1;}
        if(mouse_x > 410 && mouse_x < 505 && mouse_y > 360 && mouse_y < 385){
            fondo = menu3;
            if(mouse_b & 1){
                salida = true;
                opcion = 3;
            }
        } else {fondo = menu1;}
        blit(fondo,buffer,0,0,0,0,ancho,alto);
        draw_sprite(buffer, raton, mouse_x,mouse_y);
        blit(buffer,screen,0,0,0,0,ancho,alto);

    }
}


/**
* Clase interna del Fantasma
* @autor Esteban Muñoz.
* @create date 07/06/2015
*/
class Ghost{

    private:
        BITMAP *bmPhantom;
        BITMAP *bmBufferPhantom;
        int fx ;//Posicion en x del fantasma
        int fy ;//Posicion en y del fantasma
        int fdir ;//Dirección del fantasma
        

    public:
        Ghost(int x,int y);//Constructor
        void drawPhantom();
        void movePhantom();
        void touchPacman();
        void touchPacmanPower();
        void cambiarEstado(); 
        int estado;
};

/**
* Constructor de la interna del Fantasma
* @autor Esteban Muñoz.
* @param x posición inicial X del fantasma en la pantalla
* @param y posición inicial Y del fantasma en la pantalla
* @create date 07/06/2015
*/
Ghost::Ghost(int x,int y){
    fx = x;
    fy = y;
    estado = 0;
    fdir = rand() % 4;
    bmBufferPhantom = create_bitmap(32,32);
    bmPhantom = load_bitmap("phantom.bmp",NULL);
}

/**
* Función para validar la acción de tocar al fantasma en estado normal
* @autor Esteban Muñoz.
* @create date 07/06/2015
*/
void Ghost::touchPacman(){
    if(py == fy && px == fx || fy == antpy && fx == antpx){
        for(int i=0; i<=5; i++){
            clear(bmPacman);
            clear(bmScreen);
            drawBoard();
            blit(bmGameOver,bmPacman,i*32,0,0,0,32,32);
            draw_sprite(bmScreen,bmPacman,px,py);
            pantalla();
            rest(80);
        }
        px = 32 * 13;
        py = 32 * 10;
        dir = 4;
        vida = vida - 1;
        play_sample(muerte,100,150,1000,0);
    }
}

/**
* Función para validar la acción de tocar al fantasma en estado azul
* @autor Esteban Muñoz.
* @create date 07/06/2015
*/
void Ghost::touchPacmanPower(){
    if(py == fy && px == fx || fy == antpy && fx == antpx){
        azul = false;
        estado = 0;
        fx = 32*11;
        fy = 32*2;
        puntaje = puntaje + 50;
    }
}

/**
* Función para dibujar al fantasma
* @autor Esteban Muñoz.
* @create date 07/06/2015
*/
void Ghost::drawPhantom(){
    blit(bmPhantom,bmBufferPhantom,0,0,0,0,32,32);
    draw_sprite(bmScreen,bmPhantom,fx,fy);
    
}

/**
* Función para cambiar el estado de los fantasmas
* @autor Marco Mendoza.
* @create date 09/06/2015
*/
void Ghost::cambiarEstado(){
    if(estado == 1){
        blit(bmPhantomBlue,bmBufferPhantom,0,0,0,0,32,32);
        draw_sprite(bmScreen,bmPhantomBlue,fx,fy);
        touchPacmanPower();
    }
}

/**
* Función para mover al fantasma y validar su movimiento
* @autor Esteban Muñoz.
* @create date 07/06/2015
*/
void Ghost::movePhantom(){
    drawPhantom();
    if(estado == 0){
        touchPacman();
    }
    int tmpMuroF;

    if(estado == 1){
        cambiarEstado();
    }

    if(board[fy/32][(fx-32)/32] == '-'){
        fdir = rand() % 4;
    }
    if(fdir == 0){
        tmpMuroF = board[fy/32][(fx-32)/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fx -= 32;
    }
    if(fdir == 1){
        tmpMuroF = board[fy/32][(fx+32)/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fx += 32;
    }
    if(fdir == 2){
        tmpMuroF = board[(fy-32)/32][fx/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fy -= 32;
    }
    if(fdir == 3){
        tmpMuroF = board[(fy+32)/32][fx/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fy += 32;
    }

    if(fx <= -32){
        fx = 870;
    }
    else if (fx >= 870){
        fx = -32;
    }
}

/**
* Dibuja una palabra en una posición dada en el panel del juego
* @autor Marco Mendoza.
* @param pal[] Palabra que se desea pintar
* @param posX posición X en la pantalla
* @param posY posición Y en la pantalla
* @create date 08/06/2015
*/
void insertarPalabra(char pal[],int posX,int posY){
    int largo_palabra = largo_cadena(pal);
    int posPalabra = 0;
    for(int l=0; l<largo_palabra; l++){
        posPalabra = pixelLetra(pal[l]);

        blit(bmBufferLetras,bmLetras,posPalabra,0,0,0,32,32);
        draw_sprite(bmScreen,bmLetras,(l*32)+posX,posY);
    }
    
}

/**
* Dibuja un número en una posición dada en el panel del juego
* @autor Marco Mendoza.
* @param numero numero que se desea pintar
* @param posX posición X en la pantalla
* @param posY posición Y en la pantalla
* @create date 08/06/2015
*/
void insertarNumero(int numero,int posX,int posY){
    int posPalabra = 0;
    char Cadena[10];  // Se declara la cadena, donde almacenaremos el valor de tipo int     // Éste será el valor que pasaremos a la cadena
    sprintf(Cadena, "%d", numero);
    int largo_palabra = largo_cadena(Cadena);
    for(int l=0; l<largo_palabra; l++){
        posPalabra = pixelNumero(Cadena[l]);

        blit(bmBufferNumeros,bmNumeros,posPalabra,0,0,0,32,32);
        draw_sprite(bmScreen,bmNumeros,(l*32)+posX,posY);
    }
    
}

/**
* Devuelve un entero con la longitud de una cadena de caracteres
* @autor Marco Mendoza.
* @param cadena[] cadena de caracteres a ser calculada
* @create date 08/06/2015
*/
int largo_cadena(char cadena[])
{
    int largo=0;
    while (cadena[largo]!='\0') 
        largo++;
    return largo;
}

/**
* Devuelve un entero con su respectiva posicion en el buffer de números
* @autor Marco Mendoza.
* @param numero numero que se desea obtener su posición en el buffer
* @create date 08/06/2015
*/
int pixelNumero(char numero){
    int pos;
    if(numero == '1') pos=0*32;
    else if(numero == '2') pos=1*32;
    else if(numero == '3') pos=2*32;
    else if(numero == '4') pos=3*32;
    else if(numero == '5') pos=4*32;
    else if(numero == '6') pos=5*32;
    else if(numero == '7') pos=6*32;
    else if(numero == '8') pos=7*32;
    else if(numero == '9') pos=8*32;
    else if(numero == '0') pos=9*32;
    else if(numero == 'k') pos=10*32;
    return pos;
}

/**
* Devuelve un entero con su respectiva posicion en el buffer de letras
* @autor Marco Mendoza.
* @param letra letra que se desea obtener su posición en el buffer
* @create date 08/06/2015
*/
int pixelLetra(char letra){
    int pos;
    if(letra == 'a') pos=0*32;
    else if(letra == 'b') pos=1*32;
    else if(letra == 'c') pos=2*32;
    else if(letra == 'd') pos=3*32;
    else if(letra == 'e') pos=4*32;
    else if(letra == 'f') pos=5*32;
    else if(letra == 'g') pos=6*32;
    else if(letra == 'h') pos=7*32;
    else if(letra == 'i') pos=8*32;
    else if(letra == 'j') pos=9*32;
    else if(letra == 'k') pos=10*32;
    else if(letra == 'l') pos=11*32;
    else if(letra == 'm') pos=12*32;
    else if(letra == 'n') pos=13*32;
    else if(letra == 'o') pos=14*32;
    else if(letra == 'p') pos=15*32;
    else if(letra == 'q') pos=16*32;
    else if(letra == 'r') pos=17*32;
    else if(letra == 's') pos=18*32;
    else if(letra == 't') pos=19*32;
    else if(letra == 'u') pos=20*32;
    else if(letra == 'v') pos=21*32;
    else if(letra == 'w') pos=22*32;
    else if(letra == 'x') pos=23*32;
    else if(letra == 'y') pos=24*32;
    else if(letra == 'z') pos=25*32;
    return pos;

}

/**
* Funcion principal donde se coloca las reglas del juego
 y funcionalidades
* @autor Esteban Muñoz.
* @autor Marco Mendoza.
* @create date 31/05/2015
*/
int main(){
	allegroStart("C++PACMAN",880,600);
	bmScreen = create_bitmap(880,600);
	bmImageBoard = load_bitmap("square-icon.bmp",NULL);
	bmBufferPacman = load_bitmap("pacman.bmp",NULL);
	bmPacman = create_bitmap(32,32);
	bmFood = load_bitmap("food.bmp",NULL);
	labVertical = load_bitmap("labvertical.bmp",NULL);
    bmBufferPhantom = create_bitmap(32,32);
    bmPhantom = load_bitmap("phantom.bmp",NULL);
    bmPhantomBlue = load_bitmap("phantomAzul.bmp",NULL);
    
	labHorizontal = load_bitmap("labhorizontal.bmp",NULL);
	labarribaIzquierda = load_bitmap("labarribaizquierda.bmp",NULL);
	labarribaDerecha = load_bitmap("labarribaderecha.bmp",NULL);
	lababajoIzquierda = load_bitmap("lababajoizquierda.bmp",NULL);
	lababajoDerecha = load_bitmap("lababajoderecha.bmp",NULL);
	bmGameOver = load_bitmap("gameover.bmp",NULL);
    bmcherry = load_bitmap("cherry.bmp",NULL);

    bolitas = load_wav("Chomp.wav");
    muerte = load_wav("Dead.wav");

    bmLetras = create_bitmap(32,32);
    bmBufferLetras = load_bitmap("letras.bmp",NULL);
    bmNumeros = create_bitmap(32,32);
    bmBufferNumeros = load_bitmap("numeros.bmp",NULL);
    powerFood = load_bitmap("galletaGrande.bmp",NULL);

	BITMAP *fondoControles = load_bitmap("about.bmp", NULL);
	BITMAP *buffer = create_bitmap(880,600);
	BITMAP *buff2 = create_bitmap(880,600);
	clear_to_color(buff2,makecol(255,0,255));

    menuP M(880,600,(char *)"menu1.bmp",(char *)"menu2.bmp",(char *)"cursor.bmp",(char *)"menu3.bmp");

    bool salida, reg_menu = true;
    Ghost a(32*2,32*3);
    Ghost b(32*15,32*15);
    Ghost c(32*2,32*3);
    Ghost d(32*15,32*15);

    do{

        M.pintar();
        salida=false;
        reg_menu = true;
        if(key[KEY_ESC]){
            salida = true;
            reg_menu = false;
        }
        BITMAP *raton;
        raton = load_bitmap("cursor.bmp",NULL);        
        switch(opcion){
            case 1://Play
                
                while(!key[KEY_R] && !salida && gameOver()){

                	if(key[KEY_ESC] ){
                        salida = true;
                        reg_menu = false;
			        }

                    if(vida == 0){
                        salida = true;
                        reg_menu = false;
                    }

                    //insertarPalabra((char *)"palabra",32,32);

        			antpx = px;
        			antpy = py;
                	dir = 4;//Pacman no se mueve
                	if(key[KEY_RIGHT])		{ dir = 1; }
					else if (key[KEY_LEFT]) { dir = 0; }
					else if (key[KEY_UP])   { dir = 2; }
					else if (key[KEY_DOWN])	{ dir = 3; }
					moveRight(dir);
					moveLeft(dir);
					moveUp(dir);
					moveDown(dir);

                    clear(bmScreen);
                    drawBoard();
                    drawPacman();
                    insertarPalabra((char *)"score",32,32*17);    
                    insertarNumero(puntaje,32*6,32*17);
                    blit(bmBufferPacman,bmPacman,32,0,0,0,32,32);
                    draw_sprite(bmScreen,bmPacman,32*11,32*17);
                    insertarPalabra((char *)"x",32*12,32*17);    
                    insertarNumero(vida,32*13,32*17);  
                    //a.movePhantom();
                    //b.movePhantom();
                    c.movePhantom();
                    d.movePhantom();
                    if(azul){
                        a.estado = 1;
                        b.estado = 1;
                        c.estado = 1;
                        d.estado = 1;
                    }
                    if(cherry){
                        draw_sprite(bmScreen,bmcherry,32*16,32*17);
                    } else{
                        clear(bmcherry);
                    }
                    pantalla();

                    //Atajo
                    if(px <= -32){
                        px = 870;
                    }
                    else if (px >= 870){
                        px = -32;
                    }
                    rest(70);

					clear(bmPacman);
                    //blit(bmBufferPacman,bmPacman,32,0,0,0,32,32);
                    //draw_sprite(bmScreen,bmPacman,px,py);
                    //blit(bmScreen,screen,0,0,0,0,880,600);
					pantalla();
					rest(80);
                }
                break;
            case 2://Score
            	break;


            case 3://Controls
                while(!key[KEY_R] && !salida){
                    if(key[KEY_ESC]){
                        salida = true;
                        reg_menu = false;
                    }
                    blit(fondoControles,buffer,0,0,0,0,880,600);
                    draw_sprite(buffer, raton, mouse_x,mouse_y);
                    draw_sprite(buffer, raton, mouse_x,mouse_y);
                    blit(buffer,screen,0,0,0,0,880,600);
                }
                break;
        }
    } while(reg_menu );

    return 0;
}


/**
* Dibuja a pacman en la pantalla
* @autor Esteban Muñoz.
* @create date 05/06/2015
*/
void drawPacman(){
	blit(bmBufferPacman,bmPacman,dir*32,0,0,0,32,32);
    draw_sprite(bmScreen,bmPacman,px,py);
    rest(90);
}


void pantalla(){
    blit(bmScreen,screen,0,0,0,0,880,600);
}

/**
* Formato de la pantalla del juego para el tablero
* @autor Esteban Muñoz.
* @create date 05/06/2015
*/
void drawPhantom(){
	blit(bmPhantom,bmBufferPhantom,0,0,0,0,32,32);
	draw_sprite(bmScreen,bmPhantom,fx,fy);
}

/**
* Función alternativa del pintado del fantasma
* @autor Marco Mendoza.
* @create date 07/06/2015
*/
void drawBoard(){
	for (int i = 0; i < MAX_ROWS; i++){
		for (int j = 0; j < MAX_COLUMNS; j++){
            rest(100); //Velocidad de PACMAN
			if(board[i][j] == '*'){
				draw_sprite(bmScreen,labHorizontal,j*32,i*32);
			}
			if(board[i][j] == '+'){
				draw_sprite(bmScreen,labVertical,j*32,i*32);
			}
            if(board[i][j] == 'a'){
				draw_sprite(bmScreen,lababajoDerecha,j*32,i*32);
			}
			if(board[i][j] == 'b'){
				draw_sprite(bmScreen,lababajoIzquierda,j*32,i*32);
			}
			if(board[i][j] == 'c'){
				draw_sprite(bmScreen,labarribaDerecha,j*32,i*32);
			}
			if(board[i][j] == 'd'){
				draw_sprite(bmScreen,labarribaIzquierda,j*32,i*32);
			}
			else if(board[i][j] == '0'){
				draw_sprite(bmScreen,bmFood,j*32,i*32);
				if(py/32 == i && px/32 == j){//Cuando pacman pasa sobre la comida
                    play_sample(bolitas,300,150,1000,0);
					board[i][j] = ' ';
                    puntaje++;
                    //char c[] = puntaje++;
                    //insertarNumero((char *)puntaje,32*6,32*18);
				}
			}
			else if(board[i][j] == '?'){
                draw_sprite(bmScreen,powerFood,j*32,i*32);
                if(py/32 == i && px/32 == j){//Cuando pacman pasa sobre la comida
                    play_sample(bolitas,300,150,1000,0);
                    board[i][j] = ' ';
                    puntaje = puntaje + 10;
                    
                    azul = true;
                }
            }
            else if(board[i][j] == 'C'){
                draw_sprite(bmScreen,bmcherry,13*32,13*32);
                if(py == 13*32 && px == 13*32){//Cuando pacman pasa sobre la comida
                    board[i][j] = ' ';
                    puntaje = puntaje + 50;
                    cherry = false;
                }
            }
            if(cherry){
                if(puntaje > 50){
                    board[13][13] = 'C';
                }
            }
            rest(1000);
		}
	}
}

/**
* Pinta la pantalla para comenzar a dibujar con allegro.
* @autor Esteban Muñoz.
* @param title titulo de la pantalla.
* @param width ancho de la pantalla.
* @param height alto de la pantalla.
* @create date 03/06/2015
*/
void allegroStart(const char* title,int width,int height){
	allegro_init();
	install_mouse();
	install_keyboard();
	set_color_depth(32);
	set_window_title(title);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED,width,height,0,0);
    if(install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT,NULL)!=0){
        allegro_message("Error: Sonido");
    }
    set_volume(70,70);
}

/**
* Movimiento de pacman hacia la izquierda
* @autor Esteban Muñoz.
* @param address
* @create date 05/06/2015
*/
void moveLeft(int address){
	if(address == 0){
        int tmpMuro = board[py/32][(px-32)/32];
        if(tmpMuro == '*' || tmpMuro == 'a' || tmpMuro == 'b'
            || tmpMuro == 'c' || tmpMuro == 'd' || tmpMuro == '+')
            dir = 4;
        else
            px -= 32;
	}
}

/**
* Movimiento de pacman hacia la derecha
* @autor Esteban Muñoz.
* @param address
* @create date 05/06/2015
*/
void moveRight(int address){
	if(address == 1){
		int tmpMuro = board[py/32][(px+32)/32];
        if(tmpMuro == '*' || tmpMuro == 'a' || tmpMuro == 'b'
            || tmpMuro == 'c' || tmpMuro == 'd' || tmpMuro == '+')
            dir = 4;
        else
            px += 32;
	}
}

/**
* Movimiento de pacman hacia arriba
* @autor Esteban Muñoz.
* @param address
* @create date 05/06/2015
*/
void moveUp(int address){
	if(address == 2){
        int tmpMuro = board[(py-32)/32][px/32];
		if(tmpMuro == '*' || tmpMuro == 'a' || tmpMuro == 'b'
            || tmpMuro == 'c' || tmpMuro == 'd' || tmpMuro == '+')
            dir = 4;
        else
            py -= 32;
	}
}

/**
* Movimiento de pacman hacia abajo
* @autor Esteban Muñoz.
* @param address
* @create date 05/06/2015
*/
void moveDown(int address){
	if(address == 3){
		int tmpMuro = board[(py+32)/32][px/32];
        if(tmpMuro == '*' || tmpMuro == 'a' || tmpMuro == 'b'
            || tmpMuro == 'c' || tmpMuro == 'd' || tmpMuro == '+')
            dir = 4;
        else
            py += 32;
	}
}

/**
* Movimiento de pacman hacia arriba
* @autor Esteban Muñoz.
* @create date 08/06/2015
*/
bool gameOver(){
	for (int i = 0; i < MAX_ROWS; i++){
		for (int j = 0; j < MAX_COLUMNS; j++){
			if(board[i][j] == '0'){
				return true;
			}
		}
	}
    
	return false;
}


